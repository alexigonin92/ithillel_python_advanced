from flask import Flask
from flask import send_file
from faker import Faker
import pandas as pd

app = Flask(__name__)


@app.route('/')
def index():
    return 'It is main page'


@app.route('/requirments')
def get_requirments():
    return send_file('requirments.txt')


@app.route('/random_users')
def get_random_users():
    fake = Faker()
    return {fake.email(): fake.name() for i in range(100 + 1)}


@app.route('/avr_data')
def get_average_data():
    FROM_INCHES_TO_CENTIMETR = 2.54
    FROM_POUNDS_TO_KILOGRAM = 0.45
    data = pd.read_csv('hw.csv', sep=',')
    avr_heigh = data[' "Height(Inches)"'].mean() * FROM_INCHES_TO_CENTIMETR
    avr_weight = data[' "Weight(Pounds)"'].mean() * FROM_POUNDS_TO_KILOGRAM
    return f'Average height: {format(avr_heigh, ".2f")}\nAverage weight: {format(avr_weight, ".2f")}'


if __name__ == '__main__':
    app.run(debug=True)

